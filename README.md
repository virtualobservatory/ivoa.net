# IVOA Website

This repository contains the source code for the https://ivoa.net website.

## Contributing

The site is built using [Hugo](https://gohugo.io/), take a look at the [documentation](https://gohugo.io/getting-started/quick-start/) on how to get started.

# License

Creative Commons: Attribution-ShareAlike 4.0 International
